<?php

class AuthenticatedController extends Controller {

	protected $f3;

	function beforeroute($f3){
		//echo 'Before routing - ';
		if (!isset($_SESSION['auth']) || $_SESSION['auth']!=1)
			$this->f3->reroute('/auth');
	}
}
