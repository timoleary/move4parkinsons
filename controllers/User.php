<?php

class User extends AuthenticatedController {
	const ROLE_ADMIN      = 'admin';
	const ROLE_INSTRUCTOR = 'instructor';

	function pageList($f3) {
		//Get user Table
		$user = new DB\SQL\Mapper($f3->get('DB'),'users');
		//Query all user stores
		$users = $user->find('', array('order'=>'firstname ASC'));
		// Set results as variable to pass to template
		$f3->set('users', $users);
		$f3->set('activenav','navusers');
		$f3->set('title','User Manager');
		// Render Template
		echo \Template::instance()->render('user/list.html'); 
	}

	function pageAdd($f3) {
		// Add record just render template.
		$f3->set('activenav','navusers');
		$f3->set('validate','active');
		$f3->set('title','Add User');
		echo \Template::instance()->render('user/add.html');
	}

	function pageEdit($f3) {
		// Get the ID value from URL
		$id = $f3->get('PARAMS.id');
		// Map the DB user table
		$user=new DB\SQL\Mapper($f3->get('DB'),'users');
		// Run Query to load all values for that row
		$user->load(array('id=?',$id));
		// Copy values to POST
		$user->copyTo('POST');
		// Render Template
		$f3->set('validate','active');
		$f3->set('title','Edit User');
		$f3->set('activenav','navusers');
		echo \Template::instance()->render('user/edit.html');
	}

	function actionAddUpdate($f3) {
		$id = $f3->get('PARAMS.id');
		//create an article object
		$user=new DB\SQL\Mapper($f3->get('DB'),'users');
		//if we don't load it first Mapper will do an insert instead of update when we use save command

		if ($id) {
			$user->load(array('id=?',$id));

			$password = $f3->get('POST.password');
			if (empty($password)){
			$f3->clear('POST.password');
			}

		}
		//overwrite with values just submitted
		$user->copyFrom('POST');

		$password = $f3->get('POST.password');
		if (!empty($password)){
		$user->set('password', md5($password));
		}

		$user->save();
		// Return to admin home page, new blog entry should now be there
		$f3->reroute('/users');
	}

	function delete($f3) {
		$id = $f3->get('PARAMS.id');
		$user=new DB\SQL\Mapper($f3->get('DB'),'users');
		$user->load(array('id=?',$id));
		$user->erase();
		$f3->reroute('/users');
	}
}