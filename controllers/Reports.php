<?php

class Reports extends AuthenticatedController
{

    function pageList($f3)
    {
        $klass = new DB\SQL\Mapper($f3->get('DB'), 'classes');
        $classes = $klass->find(array('order' => 'id ASC'));

        $attendance = $f3->get('DB')->exec('SELECT DISTINCT class_id, YEAR(time) as year, MONTH(time) as month FROM `attendance` ORDER BY time ASC');

        foreach ($attendance as $key => $value) {
            $reports[$value['class_id']][$value['year']][] = ($value['month']);
        }

        $f3->set('classes', $classes);
        $f3->set('range', $reports);
        $f3->set('activenav', 'navreports');
        $f3->set('title', 'Reports Overview');
        echo \Template::instance()->render('reports/list.html');
    }


    function fixedReport($f3)
    {
        $classId = $f3->get('PARAMS.class_id');

        // Class Details
        $class = new DB\SQL\Mapper($f3->get('DB'), 'classes');
        $classdetails = $class->load(array('id=?', $classId));

        // Subscriptions to Class
        $subdetails = $f3->get('DB')->exec('SELECT subscriptions.member_id, subscriptions.class_id, subscriptions.payment_type, CONCAT(members.firstname," ", members.lastname) as membername FROM `subscriptions` INNER JOIN members ON subscriptions.member_id = members.id WHERE subscriptions.class_id=? ORDER BY membername ASC', array($classId));

        // Attendance Records
        $reportdetails = $f3->get('DB')->exec('SELECT attendance.member_id, attendance.class_id, attendance.payment_amt, attendance.sessions, attendance.payment_details, CONCAT(members.firstname," ", members.lastname) as membername FROM `attendance` INNER JOIN members ON attendance.member_id = members.id WHERE attendance.class_id=? ORDER BY attendance.sessions ASC', array($classId));

        if(count($reportdetails)==0) {
        $f3->set('title', 'No records found');
        $f3->set('activenav', 'navreports');
        echo \Template::instance()->render('reports/error.html');
        exit();
        }

        foreach ($reportdetails as $value) {
            $reformat[$value['member_id']][$value['sessions']] = $value['payment_amt'];
            $reformat[$value['member_id']]['details'] .= $value['payment_details'];
            $reformat[$value['member_id']]['balance'] += $value['payment_amt'];
        }

        $sessionsnum = $classdetails['sessions'];

        $i = 1;
        while ($i <= $sessionsnum) {
            foreach ($reformat as $value) {
                $totals[$i] += $value[$i];
            }
            $i++;
        }

        $f3->set('activenav', 'navreports');
        $f3->set('classdetails', $classdetails);
        $f3->set('sessionsnum', $sessionsnum);
        $f3->set('subdetails', $subdetails);
        $f3->set('classtotals', $totals);
        $f3->set('reportdetails', $reformat);
        $f3->set('title', $classdetails['name'].' Report');
        echo \Template::instance()->render('reports/fixed.html');
    }

    function fixedReportExport($f3)
    {
        $classId = $f3->get('PARAMS.class_id');

        // Class Details
        $class = new DB\SQL\Mapper($f3->get('DB'), 'classes');
        $classdetails = $class->load(array('id=?', $classId));

        // Subscriptions to Class
        $subdetails = $f3->get('DB')->exec('SELECT subscriptions.member_id, subscriptions.class_id, subscriptions.payment_type, CONCAT(members.firstname," ", members.lastname) as membername FROM `subscriptions` INNER JOIN members ON subscriptions.member_id = members.id WHERE subscriptions.class_id=? ORDER BY membername ASC', array($classId));

        // Attendance Records
        $reportdetails = $f3->get('DB')->exec('SELECT attendance.member_id, attendance.class_id, attendance.payment_amt, attendance.sessions, attendance.payment_details, CONCAT(members.firstname," ", members.lastname) as membername FROM `attendance` INNER JOIN members ON attendance.member_id = members.id WHERE attendance.class_id=? ORDER BY attendance.sessions ASC', array($classId));

        foreach ($reportdetails as $value) {
            $reformat[$value['member_id']][$value['sessions']] = $value['payment_amt'];
            $reformat[$value['member_id']]['details'] .= $value['payment_details'];
        }

        $sessionsnum = $classdetails['sessions'];

        $i = 1;
        while ($i <= $sessionsnum) {
            foreach ($reformat as $value) {
                $totals[$i] += $value[$i];
            }
            $i++;
        }


        $csv = 'Member,Payment Type,';
        for ($i = 1; $i <= $sessionsnum; $i++) {
            $csv .= "\"Lesson $i\",\"\",";
        }
        $csv .="Notes";
        $csv .= PHP_EOL;

        foreach ($subdetails as $value) {
            $memberid = $value['member_id'];
            $membername = $value['membername'];
            $paymenttype = $value['payment_type'];

            $type = ($paymenttype == 'directdebit') ? 'Direct Debit' : 'Cash / PAYG';

            $csv .= "\"$membername\",\"$type\",";

            for ($i = 1; $i <= $sessionsnum; $i++) {
                $paid = $reformat["$memberid"]["$i"];

                if ($paid == NULL) {
                    $attendance = "X";
                } else {
                    $attendance = "Y";
                }
                $csv .= "\"$paid\",\"$attendance\",";
            }

            $notes=$reformat["$memberid"]['details'];
            $csv .= "\"$notes\"";
            $csv .= PHP_EOL;
        }

        $csv .= ",,";
        for ($i = 1; $i <= $sessionsnum; $i++) {
            $csv .= $totals[$i] . ',"",';
        }


        $csv .= PHP_EOL;
        $csv .= PHP_EOL;
        $csv .= "Report details for " . $classdetails['name'] . " downloaded " . date("d-m-y");

        $filename = $classdetails['name'] . "-" . date("d-m-y") . ".csv";
        $exportcsv = new Helper;
        $exportcsv->exportcsv($filename, $csv);
    }


    function ongoingReport($f3)
    {
        $classId = $f3->get('PARAMS.class_id');
        $monthNum = $f3->get('PARAMS.month');
        $yearNum = $f3->get('PARAMS.year');

        $dateObj = DateTime::createFromFormat('!m', $monthNum);
        $monthName = $dateObj->format('F');
        $queryDate = "$yearNum-$monthNum-01";
        $queryDateEOM= date("Y-m-t", strtotime($queryDate));

        // Class Details
        $class = new DB\SQL\Mapper($f3->get('DB'), 'classes');
        $classdetails = $class->load(array('id=?', $classId));

        // Subscriptions to Class
        $subdetails = $f3->get('DB')->exec('SELECT DISTINCT(attendance.member_id), attendance.payment_type, attendance.class_id, CONCAT(members.firstname," ", members.lastname) as membername FROM `attendance` INNER JOIN members ON attendance.member_id = members.id WHERE attendance.class_id=? AND MONTH(attendance.time)=MONTH(?) AND YEAR(attendance.time)=? UNION SELECT subscriptions.member_id, subscriptions.payment_type, subscriptions.class_id, CONCAT(members.firstname," ", members.lastname) as membername FROM subscriptions INNER JOIN members ON subscriptions.member_id = members.id WHERE subscriptions.class_id=? AND DATE(subscriptions.signup) <= DATE(?) ORDER BY membername ASC', array($classId, $queryDate, $yearNum, $classId, $queryDateEOM));

        // Attendance Records
        $reportdetails = $f3->get('DB')->exec('SELECT attendance.member_id, attendance.payment_type, attendance.class_id, attendance.payment_amt, attendance.sessions, attendance.payment_details, CONCAT(members.firstname," ", members.lastname) as membername, DATE(attendance.time) as date FROM `attendance` INNER JOIN members ON attendance.member_id = members.id WHERE attendance.class_id=? AND MONTH(attendance.time) =MONTH(?) AND YEAR(attendance.time)=? ORDER BY attendance.sessions ASC', array($classId, $queryDate, $yearNum));


        foreach ($reportdetails as $value) {
            $reformat[$value['member_id']][$value['date']] = $value['payment_amt'];
            $reformat[$value['member_id']]['details'] .= $value['payment_details'];
            $reformat[$value['member_id']]['balance'] += $value['payment_amt'];
        }


        $reportsessions = $f3->get('DB')->exec('SELECT DISTINCT DATE(attendance.time) as date FROM `attendance` INNER JOIN members ON attendance.member_id = members.id WHERE attendance.class_id=? AND MONTH(attendance.time) =MONTH(?) AND YEAR(attendance.time)=? ORDER BY attendance.time ASC', array($classId, $queryDate, $yearNum));


        foreach($reportsessions as $value)
        {
            $sessdate=$value['date'];

            foreach ($reformat as $rkey=>$rval){
                $totals[$sessdate] += $rval[$sessdate];
            }

        }


        $f3->set('activenav', 'navreports');
        $f3->set('classdetails', $classdetails);
        $f3->set('subdetails', $subdetails);
        $f3->set('reportdetails', $reformat);
        $f3->set('classtotals', $totals);
        $f3->set('reportsessions', $reportsessions);

        $f3->set('month', $monthName);
        $f3->set('monthnum', $monthNum);
        $f3->set('year', $yearNum);
        $f3->set('title', $classdetails['name'].' Report');

        echo \Template::instance()->render('reports/ongoing.html');

    }

    function ongoingReportExport($f3)
    {
        $classId = $f3->get('PARAMS.class_id');
        $monthNum = $f3->get('PARAMS.month');
        $yearNum = $f3->get('PARAMS.year');

        $dateObj = DateTime::createFromFormat('!m', $monthNum);
        $monthName = $dateObj->format('F');
        $queryDate = "$yearNum-$monthNum-01";
        $queryDateEOM= date("Y-m-t", strtotime($queryDate));

        // Class Details
        $class = new DB\SQL\Mapper($f3->get('DB'), 'classes');
        $classdetails = $class->load(array('id=?', $classId));

        // Subscriptions to Class
        $subdetails = $f3->get('DB')->exec('SELECT DISTINCT(attendance.member_id), attendance.payment_type, attendance.class_id, CONCAT(members.firstname," ", members.lastname) as membername FROM `attendance` INNER JOIN members ON attendance.member_id = members.id WHERE attendance.class_id=? AND MONTH(attendance.time)=MONTH(?) AND YEAR(attendance.time)=? UNION SELECT subscriptions.member_id, subscriptions.payment_type, subscriptions.class_id, CONCAT(members.firstname," ", members.lastname) as membername FROM subscriptions INNER JOIN members ON subscriptions.member_id = members.id WHERE subscriptions.class_id=? AND DATE(subscriptions.signup) <= DATE(?) ORDER BY membername ASC', array($classId, $queryDate, $yearNum, $classId, $queryDateEOM));

        // Attendance Records
        $reportdetails = $f3->get('DB')->exec('SELECT attendance.member_id, attendance.payment_type, attendance.class_id, attendance.payment_amt, attendance.sessions, attendance.payment_details, CONCAT(members.firstname," ", members.lastname) as membername, DATE(attendance.time) as date FROM `attendance` INNER JOIN members ON attendance.member_id = members.id WHERE attendance.class_id=? AND MONTH(attendance.time) =MONTH(?) AND YEAR(attendance.time)=? ORDER BY attendance.sessions ASC', array($classId, $queryDate, $yearNum));

        foreach ($reportdetails as $value) {
            $reformat[$value['member_id']][$value['date']] = $value['payment_amt'];
            $reformat[$value['member_id']]['details'] .= $value['payment_details'];
            $reformat[$value['member_id']]['balance'] += $value['payment_amt'];
        }

        $reportsessions = $f3->get('DB')->exec('SELECT DISTINCT DATE(attendance.time) as date FROM `attendance` INNER JOIN members ON attendance.member_id = members.id WHERE attendance.class_id=? AND MONTH(attendance.time) =MONTH(?) AND YEAR(attendance.time)=? ORDER BY attendance.time ASC', array($classId, $queryDate, $yearNum));


        foreach($reportsessions as $value)
        {
            $sessdate=$value['date'];

            foreach ($reformat as $rkey=>$rval){
                $totals[$sessdate] += $rval[$sessdate];
            }

        }

        $csv = '"Member","Payment Type",';

        foreach($reportsessions as $value)
        {
          $sessdate=$value['date'];
          $csv .= "\"$sessdate\",,";
        }

        $csv .= "\"Balance\",";
        $csv .= "\"Notes\"";
        $csv .= PHP_EOL;

        foreach ($subdetails as $value) {
            $memberid = $value['member_id'];
            $membername = $value['membername'];
            $paymenttype = $value['payment_type'];
            $type = ($paymenttype == 'directdebit') ? 'Direct Debit' : 'Cash / PAYG';

            $csv .= "\"$membername\",\"$type\",";

            foreach($reportsessions as $value)
            {
            $sessdate=$value['date'];

            $paid = $reformat["$memberid"]["$sessdate"];

                if ($paid == NULL) {
                    $attendance = "X";
                } else {
                    $attendance = "Y";
                }

                $csv .= "\"$paid\",\"$attendance\",";

            }



            $csv .= number_format($classdetails['cost'] - $reformat["$memberid"]['balance'], 2);
            $notes=$reformat["$memberid"]['details'];
            $csv .=",\"$notes\"";
            $csv .= PHP_EOL;
        }

        $csv .= ",,";
            foreach($reportsessions as $value)
            {
            $sessdate=$value['date'];

            $csv .= $totals[$sessdate] . ",,";
             }


        $csv .= PHP_EOL;
        $csv .= PHP_EOL;
        $csv .= "Report details for " . $classdetails['name'] . " $monthName $yearNum." . PHP_EOL;
        $csv .= "Downloaded " . date("d-m-y");

        $filename = $classdetails['name'] . "-" . date("d-m-y") . ".csv";

        
        $exportcsv = new Helper;
        $exportcsv->exportcsv($filename, $csv);
    }

}
