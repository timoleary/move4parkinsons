<?php

class Adminauth extends Controller {

	function pageAuth() {
		echo \Template::instance()->render('user/auth.html'); 

	}

	function actionAuth($f3) {
		$user = new DB\SQL\Mapper($f3->get('DB'),'users');
		$auth = new \Auth($user, array('id'=>'email', 'pw'=>'password'));

		$status = $auth->login($f3->get('POST.email'), md5($f3->get('POST.password')));

		if ($status){
			$_SESSION['auth']="1";
			$this->f3->reroute('/');
		} else {
			$this->f3->reroute('/auth?invalid');
		}
	}

	function clearAuth($f3) {
		unset($_SESSION['auth']);
		$this->f3->reroute('/');
	}

}