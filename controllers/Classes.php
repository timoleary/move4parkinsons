<?php

class Classes extends AuthenticatedController
{
    const STATUS_ACTIVE = 'active';
    const STATUS_INACTIVE = 'inactive';

    function pageList($f3)
    {
        //Get class Table
        $klass = new DB\SQL\Mapper($f3->get('DB'), 'classes');
        //Query all class stores
        $all = $klass->find('', array('order' => 'name ASC'));
        $active = $klass->find(array('status=?', self::STATUS_ACTIVE), array('order' => 'name ASC'));
        $inactive = $klass->find(array('status=?', self::STATUS_INACTIVE), array('order' => 'name ASC'));
        // Set results as variable to pass to template
        $f3->set('active', $active);
        $f3->set('inactive', $inactive);
        $f3->set('all', $all);
        $f3->set('activenav', 'navclasses');
        $f3->set('title','Classes Overview');
        // Render Template
        echo \Template::instance()->render('class/list.html');
    }

    function pageAdd($f3)
    {
        // Get list of Instructors
        $user = new DB\SQL\Mapper($f3->get('DB'), 'users');
        $instructors = $user->find(array('role=?', User::ROLE_INSTRUCTOR));
        $f3->set('instructors', $instructors);
        $f3->set('activenav', 'navclasses');
        $f3->set('calendar', 'active');
        $f3->set('title','Add a new class');
        $f3->set('validate','active');
        
        echo \Template::instance()->render('class/add.html');
    }

    function pageEdit($f3)
    {
        // Get the ID value from URL
        $id = $f3->get('PARAMS.id');
        // Map the DB class table
        $klass = new DB\SQL\Mapper($f3->get('DB'), 'classes');
        // Run Query to load all values for that row
        $klass->load(array('id=?', $id));
        // Copy values to POST
        $klass->copyTo('POST');
        // Get list of Instructors
        $user = new DB\SQL\Mapper($f3->get('DB'), 'users');
        $instructors = $user->find(array('role=?', User::ROLE_INSTRUCTOR));

        // Users Subscribed to the Class
        $subdetails = $f3->get('DB')->exec('SELECT subscriptions.id, subscriptions.member_id, subscriptions.payment_type, CONCAT(members.firstname,\' \', members.lastname) as name FROM subscriptions JOIN members ON subscriptions.member_id=members.id WHERE subscriptions.class_id=? ORDER BY name ASC', array($id));

        $active = $f3->get('DB')->exec("SELECT members.id, CONCAT(members.firstname,' ', members.lastname) as name, subscriptions.class_id FROM members LEFT JOIN (SELECT * FROM subscriptions WHERE class_id=?) subscriptions ON members.id=subscriptions.member_id WHERE members.status='active' GROUP BY members.id ORDER BY name ASC", array($id));

        $f3->set('instructors', $instructors);
        $f3->set('activenav', 'navclasses');
        $f3->set('classsubs', $subdetails);
        $f3->set('subqty', count($subdetails));
        $f3->set('activemembers', $active);
        $f3->set('calendar', 'active');
        $f3->set('validate','active');
        $f3->set('title','Edit a class');

        // Render Template
        echo \Template::instance()->render('class/edit.html');
    }

    function actionAddUpdate($f3)
    {
        $id = $f3->get('PARAMS.id');
        //create an article object
        $klass = new DB\SQL\Mapper($f3->get('DB'), 'classes');
        //if we don't load it first Mapper will do an insert instead of update when we use save command
        if ($id) $klass->load(array('id=?', $id));
        //overwrite with values just submitted
        $klass->copyFrom('POST');

        $date=$klass->get('date');
        $date = DateTime::createFromFormat('d/m/Y', $date);
        $datef=$date->format('Y-m-d');
        $klass->set('date',$datef);

        $klass->save();
        // Return to admin home page, new blog entry should now be there
        $f3->reroute('/classes');
    }

    function status($f3)
    {
        $id = $f3->get('PARAMS.id');
        $klass = new DB\SQL\Mapper($f3->get('DB'), 'classes');
        $klass->load(array('id=?', $id));
        $klass->status = $klass->status == self::STATUS_ACTIVE ? self::STATUS_INACTIVE : self::STATUS_ACTIVE;
        $klass->save();
        $f3->reroute('/classes');
    }

    function export($f3)
    {
        $id = $f3->get('PARAMS.classid');

        $members = $f3->get('DB')->exec("SELECT *, subscriptions.class_id, (SELECT name FROM classes WHERE id=?) as classname FROM `members` INNER JOIN subscriptions ON members.id=subscriptions.member_id WHERE subscriptions.class_id=? ORDER BY firstname ASC", array($id, $id));

        $csv = "\"First Name\",\"Last Name\",\"Email\",\"Address 1\",\"Address 2\",\"Town/City\",\"County\",\"DOB\",\"Gender\",\"I am\",\"Mobile\",\"Landline\",\"Newsletter\",\"ICE Name\",\"ICE Mobile\",\"ICE Landline\",\"Membership #\",\"Centre Location\",\"Join Date\",\"Notes\",\"Status\",\"Enrolled Classes\"" . PHP_EOL;

        $iamstat = array(
            "I am a person with Parkinsons",
            "I am a family member of someone with Parkinsons",
            "I am a carer of someone with Parkinsons",
            "I am a health professional",
            "I'm just interested in Move4Parkinsons",
            "I would like to receive updates from Move4Parkinsons"
        );


        foreach ($members as $key => $value) {

            $firstname = $value['firstname'];
            $lastname = $value['lastname'];
            $email = $value['email'];
            $address1 = $value['address1'];
            $address2 = $value['address2'];
            $city = $value['city'];
            $county = $value['county'];
            $dob = $value['dob'];
            $gender = $value['gender'];
            $iam = $value['iam'];
            $iamextended = $iamstat[$iam];
            $mobile = $value['mobile'];
            $landline = $value['landline'];
            $newsletter = $value['newsletter'];
            $icename = $value['icename'];
            $icemobile = $value['icemobile'];
            $icelandline = $value['icelandline'];
            $membership = $value['membership'];
            $centrelocation = $value['centrelocation'];
            $joindate = $value['joindate'];
            $notes = $value['notes'];
            $status = $value['status'];
            $class = $value['classname'];

            $csv .= "\"$firstname\",\"$lastname\",\"$email\",\"$address1\",\"$address2\",\"$city\",\"$county\",\"$dob\",\"$gender\",\"$iamextended\",=\"$mobile\",=\"$landline\",\"$newsletter\",\"$icename\",=\"$icemobile\",=\"$icelandline\",\"$membership\",\"$centrelocation\",\"$joindate\",\"$notes\",\"$status\",\"$class\"";
            $csv .= PHP_EOL;
        }


        $filename = "$class Membership -" . date("d-m-y") . ".csv";
        $exportcsv = new Helper;
        $exportcsv->exportcsv($filename, $csv);

    }
}
