<?php

class Member extends AuthenticatedController
{
    const STATUS_PERSON_WITH = '0';
    const STATUS_FAMILY_MEMBER = '1';
    const STATUS_CARER = '2';
    const STATUS_HEALTH_PROFESSIONAL = '3';
    const STATUS_INTERESTED = '4';
    const STATUS_WANT_TO_BE_UPDATED = '5';

    const STATUS_ACTIVE = 'active';
    const STATUS_INACTIVE = 'inactive';

    static $statusList = Array(
        Array(
            'key' => self::STATUS_PERSON_WITH,
            'value' => "I am a person with Parkinson's"
        ),
        Array(
            'key' => self::STATUS_FAMILY_MEMBER,
            'value' => "I am a family member of someone with Parkinson's"
        ),
        Array(
            'key' => self::STATUS_CARER,
            'value' => "I am a carer of someone with Parkinson's"
        ),
        Array(
            'key' => self::STATUS_HEALTH_PROFESSIONAL,
            'value' => "I am a health professional"
        ),
        Array(
            'key' => self::STATUS_INTERESTED,
            'value' => "I'm just interested in Move4Parkinsons"
        ),
        Array(
            'key' => self::STATUS_WANT_TO_BE_UPDATED,
            'value' => "I would like to receive updates from Move4Parkinsons"
        )
    );

    function pageList($f3)
    {
        //Get member Table
        $member = new DB\SQL\Mapper($f3->get('DB'), 'members');
        //Query all members
        $active = $member->find(array('status=?', self::STATUS_ACTIVE), array('order' => 'firstname ASC'));
        $inactive = $member->find(array('status=?', self::STATUS_INACTIVE), array('order' => 'firstname ASC'));
        // Set results as variable to pass to template
        $f3->set('active', $active);
        $f3->set('inactive', $inactive);
        $f3->set('title','Membership Manager');
        $f3->set('activenav', 'navmembers');
        $f3->set('listfilter', 'active');
        echo \Template::instance()->render('member/list.html');
    }

    function pageAdd($f3)
    {
        // Add record just render template.
        $f3->set('memberStatusList', self::$statusList);
        $f3->set('activenav', 'navmembers');
        $f3->set('calendar', 'active');
        $f3->set('validate','active');
        $f3->set('title','Add a new Member');

        echo \Template::instance()->render('member/add.html');
    }

    function pageEdit($f3)
    {
        // Get the ID value from URL
        $id = $f3->get('PARAMS.id');
        // Map the DB member table
        $member = new DB\SQL\Mapper($f3->get('DB'), 'members');
        // Run Query to load all values for that row
        $member->load(array('id=?', $id));
        // Copy values to POST
        $member->copyTo('POST');

        $subdetails = $f3->get('DB')->exec('SELECT subscriptions.id, subscriptions.class_id, classes.name FROM subscriptions JOIN classes ON subscriptions.class_id= classes.id WHERE subscriptions.member_id=? ORDER BY classes.name ASC', array($id));

        $age = date_diff(date_create($member->dob), date_create('now'))->y;

        switch (true) {
            case $age <= 39:
                $bracket = '20-39';
                break;

            case $age <= 59:
                $bracket = '40-59';
                break;

            case $age <= 69:
                $bracket = '60-69';
                break;

            case $age <= 79:
                $bracket = '70-79';
                break;

            case $age <= 89:
                $bracket = '80-89';
                break;

            case $age >= 90:
                $bracket = '90+';
                break;

            default:
                //unknown
                break;
        }

        if($member->dob=='0000-00-00'){
            $bracket="Unknown";
        }


        $f3->set('memberStatusList', self::$statusList);
        // Render Template
        $f3->set('agebracket', $bracket);
        $f3->set('age', $age);
        $f3->set('activenav', 'navmembers');
        $f3->set('title','Edit Member');
        $f3->set('validate','active');
        $f3->set('subdetails', $subdetails);
        $f3->set('calendar', 'active');

        echo \Template::instance()->render('member/edit.html');
    }

    function actionAddUpdate($f3)
    {
        $id = $f3->get('PARAMS.id');
        //create an article object
        $member = new DB\SQL\Mapper($f3->get('DB'), 'members');
        //if we don't load it first Mapper will do an insert instead of update when we use save command
        if ($id) $member->load(array('id=?', $id));

        $member->copyFrom('POST');

        $dob=$member->get('dob');
        if(empty($dob)){
            $dobf='0000-00-00';
        } else {
            $dob = DateTime::createFromFormat('d/m/Y', $dob);
            $dobf=$dob->format('Y-m-d');
        }

        $member->set('dob',$dobf);

        $joindate=$member->get('joindate');
        if(empty($joindate)){
            $joindatef='0000-00-00';
        } else {
        $joindate = DateTime::createFromFormat('d/m/Y', $joindate);
        $joindatef=$joindate->format('Y-m-d');

        }
        $member->set('joindate',$joindatef);


        $member->save();
        // Return to admin home page, new blog entry should now be there
        $f3->reroute('/members');
    }

    function status($f3)
    {
        $id = $f3->get('PARAMS.id');
        $member = new DB\SQL\Mapper($f3->get('DB'), 'members');
        $member->load(array('id=?', $id));
        $member->status = $member->status == self::STATUS_ACTIVE ? self::STATUS_INACTIVE : self::STATUS_ACTIVE;
        $member->save();
        $f3->reroute('/members');
    }


    function export($f3)
    {

        $members = $f3->get('DB')->exec('SELECT *, (select GROUP_CONCAT(classes.name) from subscriptions LEFT JOIN classes ON subscriptions.class_id = classes.id where subscriptions.member_id=members.id) as subs FROM members');


        $csv = "\"First Name\",\"Last Name\",\"Email\",\"Address 1\",\"Address 2\",\"Town/City\",\"County\",\"DOB\",\"Gender\",\"I am\",\"Mobile\",\"Landline\",\"Newsletter\",\"ICE Name\",\"ICE Mobile\",\"ICE Landline\",\"Membership #\",\"Centre Location\",\"Join Date\",\"Notes\",\"Status\",\"Enrolled Classes\"" . PHP_EOL;


        foreach ($members as $key => $value) {

            $status = self::$statusList;

            $firstname = $value['firstname'];
            $lastname = $value['lastname'];
            $email = $value['email'];
            $address1 = $value['address1'];
            $address2 = $value['address2'];
            $city = $value['city'];
            $county = $value['county'];
            $dob = $value['dob'];
            $gender = $value['gender'];
            $iam = $value['iam'];
            $iamextended = $status[$iam]['value'];
            $mobile = $value['mobile'];
            $landline = $value['landline'];
            $newsletter = $value['newsletter'];
            $icename = $value['icename'];
            $icemobile = $value['icemobile'];
            $icelandline = $value['icelandline'];
            $membership = $value['membership'];
            $centrelocation = $value['centrelocation'];
            $joindate = $value['joindate'];
            $notes = $value['notes'];
            $status = $value['status'];
            $classes = $value['subs'];

            $csv .= "\"$firstname\",\"$lastname\",\"$email\",\"$address1\",\"$address2\",\"$city\",\"$county\",\"$dob\",\"$gender\",\"$iamextended\",=\"$mobile\",=\"$landline\",\"$newsletter\",\"$icename\",=\"$icemobile\",=\"$icelandline\",\"$membership\",\"$centrelocation\",\"$joindate\",\"$notes\",\"$status\",\"$classes\"";
            $csv .= PHP_EOL;
        }


        $filename = "Membership Data -" . date("d-m-y") . ".csv";
        $exportcsv = new Helper;
        $exportcsv->exportcsv($filename, $csv);

    }

}
