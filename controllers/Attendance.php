<?php

class Attendance extends AuthenticatedController {

	function pageList($f3) {
		//Get class Table
		$klass = new DB\SQL\Mapper($f3->get('DB'),'classes');
		//Query all class stores
		$classes = $klass->find('status="active"', array('order'=>'id ASC'));
		// Set results as variable to pass to template
		
		$f3->set('firstweek', date(W, strtotime(date('Y-m-01'))));
		$f3->set('lastweek', date(W, strtotime(date('Y-m-t'))));
		$f3->set('currentweek', date(W, strtotime("now")));
		$f3->set('classes', $classes);
		// Render Template
		$f3->set('activenav','navattendance');
		$f3->set('title','Attendance Overview');
		echo \Template::instance()->render('attendance/list.html'); 
	}

	function pageClassFixed($f3) {
		$classId = $f3->get('PARAMS.class_id');
		$week = $f3->get('PARAMS.week');

		$klass = new DB\SQL\Mapper($f3->get('DB'),'classes');
		//Query all class stores
		$class_ = $klass->load(array('id=?', $classId));

		$attendance = $f3->get('DB')->exec('SELECT subscriptions.member_id, subscriptions.class_id, subscriptions.payment_type, CONCAT(members.firstname," ", members.lastname) as membername, members.email, classes.name, classes.cost, attendance.time, IFNULL((SELECT SUM(attendance.payment_amt) FROM attendance WHERE attendance.member_id=subscriptions.member_id AND attendance.class_id=subscriptions.class_id),0.00) as totalpaid FROM subscriptions INNER JOIN members ON subscriptions.member_id = members.id INNER JOIN classes ON subscriptions.class_id = classes.id LEFT JOIN attendance ON subscriptions.member_id = attendance.member_id WHERE subscriptions.class_id=? GROUP BY subscriptions.member_id ORDER BY membername ASC', Array($classId));

		$checked = $f3->get('DB')->exec('SELECT DISTINCT id, member_id, payment_amt FROM `attendance` WHERE class_id=? AND sessions=?', Array($classId, $week));

		foreach($checked as $key=>$value){
			$checkedin[$value['member_id']]=$value['id'];
			$checkedinamt[$value['member_id']]=$value['payment_amt'];
		}

		// echo "<pre>";
		// print_r($checkedin);
		// print_r($checkedinamt);
		// exit();

		$f3->set('classtotal',count($attendance));
		$f3->set('checkedintotal',count($checked));
		$f3->set('class', $class_);
		$f3->set('week', $week);
		$f3->set('attendance', $attendance);
		$f3->set('checkedin', $checkedin);
		$f3->set('checkedinamt', $checkedinamt);
		$f3->set('activenav','navattendance');
		$f3->set('listfilter','active');
		$f3->set('title', $class_['name'].' class attendance');
		echo \Template::instance()->render('attendance/fixed.html'); 
	}

		function pageClassOngoing($f3) {
		$classId = $f3->get('PARAMS.class_id');
		$date = $f3->get('PARAMS.date');

		$klass = new DB\SQL\Mapper($f3->get('DB'),'classes');
		//Query all class stores
		$class_ = $klass->load(array('id=?', $classId));

		$attendance = $f3->get('DB')->exec('SELECT subscriptions.member_id, subscriptions.class_id, subscriptions.payment_type, CONCAT(members.firstname," ", members.lastname) as membername, members.email, classes.name, classes.cost, attendance.time, IFNULL((SELECT SUM(attendance.payment_amt) FROM attendance WHERE MONTH(attendance.time) = MONTH(?) AND attendance.member_id=subscriptions.member_id AND attendance.class_id=subscriptions.class_id),0.00) as totalpaid FROM subscriptions INNER JOIN members ON subscriptions.member_id = members.id INNER JOIN classes ON subscriptions.class_id = classes.id LEFT JOIN attendance ON subscriptions.member_id = attendance.member_id WHERE subscriptions.class_id=? GROUP BY subscriptions.member_id ORDER BY membername ASC', Array($date, $classId));

		// var_dump($attendance);
		// die;
		

		$checked = $f3->get('DB')->exec("SELECT DISTINCT id, member_id, payment_amt FROM attendance WHERE class_id=? AND date(time)=?", Array($classId,$date));

		// print_r($checked);
		// exit;
		foreach($checked as $key=>$value){
			$checkedin[$value['member_id']]=$value['id'];
			$checkedinamt[$value['member_id']]=$value['payment_amt'];
		}

		$f3->set('classtotal',count($attendance));
		$f3->set('checkedintotal',count($checkedin));
		$f3->set('class', $class_);
		$f3->set('date', $date);
		$f3->set('attendance', $attendance);
		$f3->set('checkedin', $checkedin);
		$f3->set('checkedinamt', $checkedinamt);
		$f3->set('activenav','navattendance');
		$f3->set('title', $class_['name'].' class attendance');
		$f3->set('listfilter','active');


		echo \Template::instance()->render('attendance/ongoing.html'); 
	}

	function actionAdd($f3) {

		//print_r($f3->get('POST'));

		$classId = $f3->get('POST.class_id');
		$memberId = $f3->get('POST.member_id');
		$week = $f3->get('POST.week');
		$payment_type = $f3->get('POST.payment_type');
		$payment_details = $f3->get('POST.payment_details');
		$payment_amt = $f3->get('POST.payment_amt');
		$classtype = $f3->get('POST.classtype');
		$date = $f3->get('POST.date');

		//create an article object
		$attendance = new DB\SQL\Mapper($f3->get('DB'),'attendance');
		//if we don't load it first Mapper will do an insert instead of update when we use save command

		//overwrite with values just submitted
		$attendance->set('class_id', $classId);
		$attendance->set('member_id', $memberId);
		$attendance->set('sessions', $week);
		$attendance->set('payment_type', $payment_type);
		$attendance->set('payment_details', $payment_details);
		$attendance->set('payment_amt', $payment_amt);

		if ($date!=''){
		$attendance->set('time', $date.' '.date('H:i:s'));
		}


		$attendance->save();

		if($classtype=="ongoing"){
		$f3->reroute("/attendance/ongoing/$classId/$date");
		} else {
		$f3->reroute("/attendance/$classId/$week");
		}

	}

	function delete($f3) {
		$id = $f3->get('PARAMS.id');
		$classid=$f3->get('PARAMS.classid');
		$week=$f3->get('PARAMS.week');
		$classtype=$f3->get('PARAMS.classtype');

		$attendance=new DB\SQL\Mapper($f3->get('DB'),'attendance');
		$attendance->load(array('id=?',$id));
		$attendance->erase();

		if($classtype=="ongoing"){
		$f3->reroute("/attendance/ongoing/$classid/$week");
		} else {
		$f3->reroute("/attendance/$classid/$week");
		}

	}

	function retroList($f3) {
		$klass = new DB\SQL\Mapper($f3->get('DB'),'classes');
		$classes = $klass->find('status="active"', array('order'=>'id ASC'));

		$f3->set('classes', $classes);
		$f3->set('calendar', 'active');
		// Render Template
		$f3->set('activenav','navattendance');
		$f3->set('title','Retrospective Attendance');
		$f3->set('validate','active');

		echo \Template::instance()->render('attendance/retro.html');
	}

	function retrospective($f3) {

	$classid=$f3->get('POST.classid');
	$date=$f3->get('POST.date');

	$datef = DateTime::createFromFormat('d/m/Y', $date);
	$dateiso=$datef->format('Y-m-d');

	$f3->reroute("/attendance/ongoing/$classid/$dateiso");

	}
}
