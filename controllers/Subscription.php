<?php

class Subscription extends AuthenticatedController {
	function pageList($f3) {
		//Get subscription Table
		// $subscription = new DB\SQL\Mapper($f3->get('DB'),'subscriptions');
		// //Query all subscription stores
		// $subscriptions = $subscription->find('', array('order'=>'id ASC'));
		// echo '<pre>';
		// var_dump($subscription);die;

		$subscriptions = $f3->get('DB')->exec('SELECT s.id AS `id`, c.name AS `cname`, m.firstname AS `mfirstname`, m.lastname AS `mlastname` FROM subscriptions AS `s` LEFT JOIN members AS `m` ON m.id=s.member_id LEFT JOIN classes AS `c` ON c.id=s.class_id');
		// echo '<pre>';
		// var_dump($subscriptions);die;
		$f3->set('subscriptions', $subscriptions);

		// Render Template
		echo \Template::instance()->render('subscription/list.html'); 
	}

	function actionClaimPayment($f3) {
		
	}

	function pageAdd($f3) {
		// Get list of Member
		$member = new DB\SQL\Mapper($f3->get('DB'),'members');
		//Query all member stores
		$members = $member->find(array('status=?', Member::STATUS_ACTIVE), array('order'=>'id ASC'));
		// Set results as variable to pass to template
		$f3->set('members', $members);

		// Get list of classes
		$klass = new DB\SQL\Mapper($f3->get('DB'),'classes');
		$classes = $klass->find(array('status=?', Classes::STATUS_ACTIVE), array('order'=>'id ASC'));
		$f3->set('classes', $classes);

		// Add record just render template
		echo \Template::instance()->render('subscription/add.html');
	}

	function actionAddUpdate($f3) {
		$id = $f3->get('PARAMS.id');
		$classid=$f3->get('POST.class_id');

		$sms = $f3->get('POST.sendsms');
		if ($sms==1) {
			$sms=new Sendmode('user','pass','Move4Parkinsons');
			$result=$sms->send('phonenum','Thank you for signing up for our singing class');
		}
		//create an article object
		$subscription=new DB\SQL\Mapper($f3->get('DB'),'subscriptions');
		//if we don't load it first Mapper will do an insert instead of update when we use save command
		if ($id) $subscription->load(array('id=?',$id));
		//overwrite with values just submitted
		$subscription->copyFrom('POST');
		$subscription->save();
		// Return to admin home page, new blog entry should now be there
		$f3->reroute("/class/edit/$classid");
	}

/*	function delete($f3) {
		$id = $f3->get('PARAMS.id');
		$classid=$f3->get('PARAMS.classid');
		$subscription=new DB\SQL\Mapper($f3->get('DB'),'subscriptions');
		$subscription->load(array('id=?',$id));
		$subscription->erase();
		$f3->reroute("/class/edit/$classid");
	}*/

	function delete($f3) {
		$id = $f3->get('PARAMS.id');
		$classid=$f3->get('PARAMS.classid');
		$memberid=$f3->get('PARAMS.memberid');
		$subscription=new DB\SQL\Mapper($f3->get('DB'),'subscriptions');
		$subscription->load(array('id=?',$id));
		$subscription->erase();

		if($memberid){
		$f3->reroute("/member/edit/$memberid");
		} else {
		$f3->reroute("/class/edit/$classid");
		}
	}
}