<?php

class Helper
{

    function exportcsv($filename, $data)
    {
        header('Content-Encoding: UTF-8');
        header('Content-type: text/csv; charset=UTF-8');
        header("Content-Disposition: attachment; filename=\"$filename\"");
        header("Pragma: no-cache");
        header("Expires: 0");
        echo "\xEF\xBB\xBF"; // UTF-8 BOM
        echo $data;
        exit();
    }

    static public function renderdate($args)
    {

      $attr = $args['@attrib'];
      $date=$attr['data'];

      $tmpl = \Preview::instance();
      $resolveddate=$tmpl->resolve($date);

      return date_format(date_create($resolveddate),'d/m/Y');

    }

}