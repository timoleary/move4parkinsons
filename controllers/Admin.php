<?php

class Admin extends AuthenticatedController {
	// function beforeroute(){
	// 	if (!isset($_SESSION['auth']) || $_SESSION['auth']!=1)
	// 		$this->f3->reroute('/adminz/auth');
	// }

	function home($f3) {
		// Render Template

		$overview = $f3->get('DB')->exec('SELECT status, count(status) as statusc FROM members GROUP BY status ORDER BY status ASC');

		foreach($overview as $key=>$value){
			$overviewdata[$value['status']]=$value['statusc'];
		}

		$iambreakdown = $f3->get('DB')->exec('SELECT iam, count(iam) as groupc FROM members GROUP BY iam ORDER BY iam ASC');
		$iamlabels=array('Person with','Family Member','Carer','Health Professional','Interested','Want to be updated');

		foreach($iambreakdown as $key=>$value){
			$iamdata[]=$value['groupc'];
		}

		$classbreakdown = $f3->get('DB')->exec("SELECT COUNT(subscriptions.id) as subs, classes.name FROM subscriptions JOIN classes ON class_id=classes.id WHERE classes.status='active' GROUP BY class_id");

		foreach($classbreakdown as $key=>$value){
			$classlabels[]=$value['name'];
			$classdata[]=$value['subs'];
		}
		
		$colors=array('#F58307','#FBBB05','#DE9005','#DE5605','#FB3E05','#753F03','#755027','#F7A751');

		$f3->set('title',"Move4Parkinsons - Membership & Class Administration");
		$f3->set('activenav',"navhome");
		$f3->set('colors', $colors);
		$f3->set('overview',$overviewdata);
		$f3->set('iamdata', $iamdata);
		$f3->set('iamlabels', $iamlabels);
		$f3->set('classdata', $classdata);
		$f3->set('classlabels', $classlabels);

		echo \Template::instance()->render('home.html'); 
	}

	function check($f3) {
		throw new Exception('Not implemented');
	}
}
