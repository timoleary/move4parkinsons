<?php

class Controller {
	
	protected $f3;

	function __construct() {
		@session_start();
		$f3=Base::instance();
		$this->f3=$f3;

		$dbname=$f3->get('DBNAME');
		$dbuser=$f3->get('DBUSER');
		$dbpass=$f3->get('DBPASS');

		$f3->set('DB', new DB\SQL("mysql:host=localhost;port=3306;dbname=$dbname",$dbuser,$dbpass));
	}

}