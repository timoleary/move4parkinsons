<?php
error_reporting( E_ALL );
require_once('vendor/autoload.php');

$f3 = \Base::instance();
$f3->config('config.ini');

$f3->route('GET|HEAD /','Admin->home');
$f3->route('GET|HEAD /auth','Adminauth->pageAuth');
$f3->route('POST /auth','Adminauth->actionAuth');
$f3->route('GET /signout','Adminauth->clearAuth');

// Users -----------------------------------------------------------------------

// Overview
$f3->route('GET /users','User->pageList');

// Adding
$f3->route('GET|HEAD /user/add','User->pageAdd');
$f3->route('POST /user/add','User->actionAddUpdate');

// Editing
$f3->route('GET|HEAD /user/edit/@id','User->pageEdit');
$f3->route('POST /user/edit/@id','User->actionAddUpdate');

// Deleting
$f3->route('GET|HEAD /user/delete/@id','User->delete');

// Members ---------------------------------------------------------------------

// Overview
$f3->route('GET /members','Member->pageList');

// Adding
$f3->route('GET|HEAD /member/add','Member->pageAdd');
$f3->route('POST /member/add','Member->actionAddUpdate');

// Editing
$f3->route('GET|HEAD /member/edit/@id','Member->pageEdit');
$f3->route('POST /member/edit/@id','Member->actionAddUpdate');

// Status Toggle
$f3->route('GET|HEAD /member/status/@id','Member->status');

// Status Toggle
$f3->route('GET|HEAD /members/export','Member->export');

// Subscribers -----------------------------------------------------------------

// Overview
$f3->route('GET /subscribers','Subscribers->pageList');

// Adding
$f3->route('GET|HEAD /subscribers/add','Subscribers->pageAdd');
$f3->route('POST /subscribers/add','Subscribers->actionAddUpdate');

// Editing
$f3->route('GET|HEAD /subscribers/edit/@id','Subscribers->pageEdit');
$f3->route('POST /subscribers/edit/@id','Subscribers->actionAddUpdate');

// Status Toggle
$f3->route('GET|HEAD /subscribers/status/@id','Subscribers->status');

// Status Toggle
$f3->route('GET|HEAD /subscribers/export','Subscribers->export');

// Classes ---------------------------------------------------------------------

// Overview
$f3->route('GET /classes','Classes->pageList');

// Adding
$f3->route('GET|HEAD /class/add','Classes->pageAdd');
$f3->route('POST /class/add','Classes->actionAddUpdate');

// Editing
$f3->route('GET|HEAD /class/edit/@id','Classes->pageEdit');
$f3->route('POST /class/edit/@id','Classes->actionAddUpdate');

// Toggle Status
$f3->route('GET|HEAD /class/status/@id','Classes->status');

// Export
$f3->route('GET|HEAD /class/export/@classid','Classes->export');

// Classes ---------------------------------------------------------------------

// Overview
$f3->route('GET /subscriptions','Subscription->pageList');

// Adding
$f3->route('GET|HEAD /subscription/add','Subscription->pageAdd');
$f3->route('POST /subscription/add','Subscription->actionAddUpdate');

// Deleting
$f3->route('GET|HEAD /subscription/delete/@classid/@id','Subscription->delete');
$f3->route('GET|HEAD /subscription/delete/@classid/@id/@memberid','Subscription->delete');


// Attendance ------------------------------------------------------------------

// Overview
$f3->route('GET /attendance','Attendance->pageList');
// Logging Attendance
$f3->route('GET /attendance/@class_id/@week','Attendance->pageClassFixed');
$f3->route('GET /attendance/ongoing/@class_id/@date','Attendance->pageClassOngoing');
// Adding
$f3->route('POST /attendance','Attendance->actionAdd');
// Remove
$f3->route('GET|HEAD /attendance/delete/@classtype/@classid/@week/@id','Attendance->delete');
// Retrospective
// // Retrospective
$f3->route('GET|HEAD /attendance/retrospective','Attendance->retroList');
$f3->route('POST /attendance/retro','Attendance->retrospective');

// Reports ---------------------------------------------------------------------

$f3->route('GET /reports','Reports->pageList');

$f3->route('GET /reports/@class_id','Reports->fixedReport');
$f3->route('GET /reports/export/@class_id','Reports->fixedReportExport');

$f3->route('GET /reports/ongoing/@year/@month/@class_id','Reports->ongoingReport');
$f3->route('GET /reports/ongoing/export/@year/@month/@class_id','Reports->ongoingReportExport');

// PayPal ----------------------------------------------------------------------

$f3->route('GET /paypal/setup/@memberid','Billing->setup');
$f3->route('GET /paypal/complete/@memberid','Billing->createba');
$f3->route('GET /thankyou','Billing->thankyou');



// $f3->set('ONERROR',function($f3){
// $f3->reroute('/');
// });

$f3->run();